import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
// import { HomePage } from '../pages/home/home';
// import { ListPage } from '../pages/list/list';
import { EventsPage } from '../pages/events/events';
import { CurrentsPage } from '../pages/currents/currents';
import { FishCountsPage } from '../pages/fish-counts/fish-counts';
import { FishingExchangePage } from '../pages/fishing-exchange/fishing-exchange';
import { FishingNewsPage } from '../pages/fishing-news/fishing-news';
import { MarineWeatherPage } from '../pages/marine-weather/marine-weather';
import { ShoutOutPage } from '../pages/shout-out/shout-out';
import { TidesPage } from '../pages/tides/tides';
import { WhatsHotPage } from '../pages/whats-hot/whats-hot';
import { NotificationPage } from '../pages/notification/notification';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    // HomePage,
    //ListPage,
    EventsPage,
    CurrentsPage,
    FishCountsPage,
    FishingExchangePage,
    FishingNewsPage,
    MarineWeatherPage,
    ShoutOutPage,
    TidesPage,
    WhatsHotPage,
    NotificationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // HomePage,
    //ListPage,
    EventsPage,
    CurrentsPage,
    FishCountsPage,
    FishingExchangePage,
    FishingNewsPage,
    MarineWeatherPage,
    ShoutOutPage,
    TidesPage,
    WhatsHotPage,
    NotificationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
