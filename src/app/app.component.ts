import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { HomePage } from '../pages/home/home';
// import { ListPage } from '../pages/list/list';
import { EventsPage } from '../pages/events/events';
import { CurrentsPage } from '../pages/currents/currents';
import { FishCountsPage } from '../pages/fish-counts/fish-counts';
import { FishingExchangePage } from '../pages/fishing-exchange/fishing-exchange';
import { FishingNewsPage } from '../pages/fishing-news/fishing-news';
import { MarineWeatherPage } from '../pages/marine-weather/marine-weather';
import { ShoutOutPage } from '../pages/shout-out/shout-out';
import { TidesPage } from '../pages/tides/tides';
import { WhatsHotPage } from '../pages/whats-hot/whats-hot'; 
import { NotificationPage } from '../pages/notification/notification'; 

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = EventsPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      // { title: 'Home', component: HomePage },
      //{ title: 'List', component: ListPage },
      { title: 'Currents', component: CurrentsPage },
      { title: 'Fish Counts', component: FishCountsPage },
      { title: 'Fishing Exchange', component: FishingExchangePage },
      { title: 'Fishing News', component: FishingNewsPage },
      { title: 'Marine Weather', component: MarineWeatherPage },
      { title: 'Shout Out', component: ShoutOutPage },
      { title: 'Tides', component: TidesPage },
      { title: 'Whats Hot', component: WhatsHotPage },
      { title: 'Notification', component: NotificationPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
