import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishingNewsPage } from './fishing-news';

@NgModule({
  declarations: [
    FishingNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(FishingNewsPage),
  ],
})
export class FishingNewsPageModule {}
