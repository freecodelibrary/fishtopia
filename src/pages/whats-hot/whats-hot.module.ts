import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WhatsHotPage } from './whats-hot';

@NgModule({
  declarations: [
    WhatsHotPage,
  ],
  imports: [
    IonicPageModule.forChild(WhatsHotPage),
  ],
})
export class WhatsHotPageModule {}
