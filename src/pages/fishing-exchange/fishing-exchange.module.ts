import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishingExchangePage } from './fishing-exchange';

@NgModule({
  declarations: [
    FishingExchangePage,
  ],
  imports: [
    IonicPageModule.forChild(FishingExchangePage),
  ],
})
export class FishingExchangePageModule {}
