import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarineWeatherPage } from './marine-weather';

@NgModule({
  declarations: [
    MarineWeatherPage,
  ],
  imports: [
    IonicPageModule.forChild(MarineWeatherPage),
  ],
})
export class MarineWeatherPageModule {}
