import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrentsPage } from './currents';

@NgModule({
  declarations: [
    CurrentsPage,
  ],
  imports: [
    IonicPageModule.forChild(CurrentsPage),
  ],
})
export class CurrentsPageModule {}
