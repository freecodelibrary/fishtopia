import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishCountsPage } from './fish-counts';

@NgModule({
  declarations: [
    FishCountsPage,
  ],
  imports: [
    IonicPageModule.forChild(FishCountsPage),
  ],
})
export class FishCountsPageModule {}
