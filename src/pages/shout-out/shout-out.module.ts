import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoutOutPage } from './shout-out';

@NgModule({
  declarations: [
    ShoutOutPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoutOutPage),
  ],
})
export class ShoutOutPageModule {}
