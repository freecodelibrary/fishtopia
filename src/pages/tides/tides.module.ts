import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TidesPage } from './tides';

@NgModule({
  declarations: [
    TidesPage,
  ],
  imports: [
    IonicPageModule.forChild(TidesPage),
  ],
})
export class TidesPageModule {}
